package weatherapp;

/**
 * @author Jakub Jerzyński
 */
public class Weather {
    
    private String weathernfo;
    private String temperature;
    private String minTemperature;
    private String maxTemperature;
    private String pressure;
    private String humidity;
    private String wind;
    private String cityNameFromAPI;
    private String countrySymbolFromAPI;
    
    private String[] split;
    private String temporary;
    
    Weather(String response){
        setTemperature(response);
        setMinTemperature(response);
        setMaxTemperature(response);
        setPressure(response);
        setHumidity(response);
        setWind(response);
        setWeathernfo(response);
        setCityNameFromAPI(response);
        setCountrySymbolFromAPI(response);
        
    }

    protected String getWeathernfo() {
        return weathernfo;
    }

    private void setWeathernfo(String response) {
        split = response.split("\"description\":", 0);
        temporary = split[1];
        this.weathernfo = temporary.split(",")[0];
    }

    protected String getTemperature() {
        return temperature;
    }

    private void setTemperature(String response) {
        split = response.split("\"temp\":", 0);
        temporary = split[1];
        this.temperature = temporary.split(",")[0];
    }

    protected String getMinTemperature() {
        return minTemperature;
    }

    private void setMinTemperature(String response) {
        split = response.split("\"temp_min\":", 0);
        temporary = split[1];
        this.minTemperature = temporary.split(",")[0];
    }

    protected String getMaxTemperature() {
        return maxTemperature;
    }

    private void setMaxTemperature(String response) {
        split = response.split("\"temp_max\":", 0);
        temporary = split[1];
        this.maxTemperature = temporary.split(",")[0];
    }

    protected String getPressure() {
        return pressure;
    }

    private void setPressure(String response) {
        split = response.split("\"pressure\":", 0);
        temporary = split[1];
        this.pressure = temporary.split(",")[0];
    }

    protected String getHumidity() {
        return humidity;
    }

    private void setHumidity(String response) {
        split = response.split("\"humidity\":", 0);
        temporary = split[1];
        this.humidity = temporary.split(",")[0];
    }

    protected String getWind() {
        return wind;
    }

    private void setWind(String response) {
        split = response.split("\"speed\":", 0);
        temporary = split[1];
        this.wind = temporary.split(",")[0];
    }

    protected String getCityNameFromAPI() {
        return cityNameFromAPI;
    }

    private void setCityNameFromAPI(String response) {
        split = response.split("\"name\":", 0);
        temporary = split[1];
        this.cityNameFromAPI = temporary.split(",")[0];
    }

    protected String getCountrySymbolFromAPI() {
        return countrySymbolFromAPI;
    }

    private void setCountrySymbolFromAPI(String response) {
        split = response.split("\"country\":", 0);
        temporary = split[1];
        this.countrySymbolFromAPI = temporary.split(",")[0];
    }
    
    
    
}
