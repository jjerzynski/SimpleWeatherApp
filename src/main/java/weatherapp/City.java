package weatherapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Jakub Jerzyński
 */
public class City {

    private String defaultCity;
    private String cityName;
    private String countrySymbol;
    
    City(){   
    }
    
    City(String cityName){
        this.cityName = cityName;
    }
    
    City(String cityName, String countrySymbol){
        this.cityName = cityName;
        this.countrySymbol = countrySymbol;
    }

    protected String getCityName() {
        return cityName;
    }

    protected void setCityName(String cityName) {
        this.cityName = cityName;        
    }
    
    protected void setCountrySymbol(String countrySymbol) {
        this.countrySymbol = countrySymbol;
    }

    protected String getCountrySymbol() {
        return countrySymbol;
    }

    protected void setDefaultCity(String defaultCity) throws IOException{
        this.defaultCity = defaultCity;
        String fileName = "resources\\defaultcity.txt";
        
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))){
            bw.write(defaultCity);
            bw.flush();
            bw.close();
        } catch (IOException exc){
            System.out.println("Nie mogę ustawić domyślnego miasta!");
        } 
    }

    protected String getDefaultCity() {
        String readCity;
        
        try (BufferedReader cityReader = new BufferedReader(new FileReader("resources\\defaultcity.txt"))) {
            while ((readCity = cityReader.readLine()) != null) {
                defaultCity = readCity;
            }
        } catch (IOException exc) {
            System.out.println("Błąd wczytywania pliku defaultcity.txt");
        } 
        return defaultCity;
    }
     
}

