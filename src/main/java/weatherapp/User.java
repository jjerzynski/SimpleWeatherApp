package weatherapp;

/**
 * @author Kolczasty
 */
public class User {
    
    private String id;
    
    User(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
