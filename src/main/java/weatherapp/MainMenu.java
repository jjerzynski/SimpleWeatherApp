package weatherapp;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Jakub Jerzyński
 */
public class MainMenu {

    private final String DEF_COLOUR = "\u001B[0m";
    private final String RED_COLOUR = "\u001B[31m";
    private final String GREEN_COLOUR = "\u001B[32m";
    private final String BLUE_COLOUR = "\u001B[34m";
    private final String YELLOW_COLOUR = "\u001B[33m";

    private String cityName;
    private String defaultCity;
    private String response;
    private String responseString;
    private String currentUnitSystem = "metric";
    private int internetConnectionErrorCounter = 3;
    private boolean defCityWeatherChecked = false;

    private final City city = new City();
    private final RestResources resources = new RestResources(currentUnitSystem);
    private final Logo logo = new Logo();


    MainMenu() throws IOException, InterruptedException {
        this.defaultCity = city.getDefaultCity();
        showMainMenu();
    }

    //Main Menu Methods
    private void showMainMenu() throws IOException, InterruptedException {  //TODO

        screenCleaner();
        logo.showLogo();

        System.out.print("\tMiasto: ");
        showWeatherForDefaultCity(defaultCity.toUpperCase());

        System.out.println("\n-----------------------------------------------------------------------------\n");

        mainMenuOptions();
    }

    private void mainMenuOptions() throws IOException, InterruptedException { //TODO? Z tekstu?
        System.out.print("\tMENU GŁÓWNE\n\n"
                + "1. Sprawdź pogodę dla wybranego miasta\n"
                + "2. Opcje" + "\t(Obecne ustawienia - twoje miasto: " + city.getDefaultCity() + ", jednostki: "
                    + resources.getUnitSysForUser() + ")\n"
                + "3. Opis działania programu\n"
                + "4. Zakończ program\n"
                + "\n"
                + "Wybierz opcję i naciśnij [Enter]:\n");

        mainMenuChoice();
    }

    private void mainMenuChoice() throws IOException, InterruptedException {
        int mainMenuChoice = 0;

        try {
            System.out.print("=> ");
            mainMenuChoice = new Scanner(System.in).nextInt();
            executeMainMenuChoice(mainMenuChoice);
        } catch (InputMismatchException exc) {
            executeMainMenuChoice(mainMenuChoice);
        }
    }

    private void executeMainMenuChoice(int mainMenuChoice) throws IOException, InterruptedException {
        switch (mainMenuChoice) {
            case 1:
                chceckCity();
                break;
            case 2:
                appOptions();
                break;
            case 3:
                applicationInfo();
                break;
            case 4:
                System.exit(0);
                break;
            default:
                System.out.print("Błąd! Podałeś opcję z niedozwolonego zakresu!");
                mainMenuChoice();
                break;
        }
    }

    //Main Menu -> Chceck Weather in a city from User input
    private void chceckCity() throws IOException, InterruptedException {

        screenCleaner();
        logo.showLogo();

        System.out.println("Podaj miasto (bez znaków dialektycznych i spacji w nazwie np. Wroclaw, SanFrancisco):");
        userCityName();
        showWeatherForCity(city.getCityName());
        menuAfterChceckCity();
    }

    private void userCityName() {
        System.out.print("=> ");
        cityName = new Scanner(System.in).nextLine();
        city.setCityName(cityName);
    }

    private void menuAfterChceckCity() throws IOException, InterruptedException {

        System.out.println("OPCJE\n\n"
                + "1. Sprawdź dla innego miasta\n"
                + "2. Wróć do menu głównego\n"
                + "3. Zakończ program\n");

        menuAfterChceckCityChoice();

    }

    private void menuAfterChceckCityChoice() throws IOException, InterruptedException {
        int menuAfterChceckCityChoice = 0;

        try {
            System.out.print("=> ");
            menuAfterChceckCityChoice = new Scanner(System.in).nextInt();
            executeAfterChceckCityChoice(menuAfterChceckCityChoice);
        } catch (InputMismatchException exc) {
            executeAfterChceckCityChoice(menuAfterChceckCityChoice);
        }

    }

    private void executeAfterChceckCityChoice(int menuAfterChceckCityChoice) throws IOException, InterruptedException {

        switch (menuAfterChceckCityChoice) {
            case 1:
                chceckCity();
                break;
            case 2:
                showMainMenu();
                break;
            case 3:
                System.exit(0);
                break;
            default:
                menuAfterChceckCityChoice();
                break;
        }
    }

    //Main Menu -> Application Options
    private void appOptions() throws IOException, InterruptedException {

        screenCleaner();
        logo.showLogo();

        System.out.println("OPCJE\n\n"
                + "1. Ustaw miasto domyślne\n"
                + "\t Obecne miasto: " + city.getDefaultCity() + "\n"
                + "2. Ustaw system jednostek\n"
                + "\t Obecne jednostki: " + resources.getUnitSysForUser() + "\n"
                + "3. Ustaw ID uzytkownika\n"
                + "\t Obecne ID: " + resources.getUserId() + "\n"
                + "4. Powrót do głównego menu\n");

        appOptionsChoice();
    }

    private void appOptionsChoice() throws IOException, InterruptedException {
        int appOptionsChoice = 0;

        try {
            System.out.print("=> ");
            appOptionsChoice = new Scanner(System.in).nextInt();
            executeAppOptionsChoice(appOptionsChoice);
        } catch (InputMismatchException exc) {
            executeAppOptionsChoice(appOptionsChoice);
        }
    }

    private void executeAppOptionsChoice(int appOptionsChoice) throws IOException, InterruptedException {

        switch (appOptionsChoice) {
            case 1:
                setDefaultCity();
                break;
            case 2:
                weatherUnitsOptions();
                break;
            case 3:
                userIdOptions();
                break;
            case 4:
                showMainMenu();
                break;
            default:
                appOptionsChoice();
                break;
        }
    }

    //Application Option -> Set Default City
    private void setDefaultCity() throws IOException, InterruptedException {

        screenCleaner();
        logo.showLogo();

        System.out.print("Podaj domyślne miasto, które będzie wyświetlać się za każdym"
                + "uruchomieniem aplikacji: ");
        try {
            System.out.print("=> ");
            defaultCity = new Scanner(System.in).nextLine();
            city.setDefaultCity(defaultCity);
            defCityWeatherChecked = false;

        } catch (IOException exc) {
            System.out.println("Wystąpił błąd zapisu nowego miasta, domyślne miasto"
                    + "pozostaje bez zmian: " + city.getDefaultCity());
            city.setDefaultCity(city.getDefaultCity());
        }

        appOptions();
    }

    //Application Option -> Change Unit System (metric/imperial)
    private void weatherUnitsOptions() throws IOException, InterruptedException {

        screenCleaner();
        logo.showLogo();

        System.out.print("Które jednostki miar chcesz przyjąć?\n"
                + "1. System metryczny (mi. Stopnie Celcjusza)\n"
                + "2. System imperialny (mi. Stopnie Farenheita)\n"
                + "3. Wróć do opcji\n"
                + "\n");
        System.out.println("Wybierz odpowiednią opcję i naciśnij [enter]: ");

        weatherUnitsOptionsChoice();
    }

    private void weatherUnitsOptionsChoice() throws IOException, InterruptedException {
        int unitSystem = 0;
        System.out.print("=> ");

        try {
            unitSystem = new Scanner(System.in).nextInt();
            setWeatherUnits(unitSystem);
        } catch (InputMismatchException exc) {
            System.out.println("Podaj odpowiednią opcję");
            setWeatherUnits(unitSystem);
        }
    }

    private void setWeatherUnits(int unitSystemChoice) throws IOException, InterruptedException {

        switch (unitSystemChoice) {
            case 1:
                currentUnitSystem = "metric";
                defCityWeatherChecked = false;
                resources.setUnitSystem(currentUnitSystem);
                break;
            case 2:
                currentUnitSystem = "imperial";
                defCityWeatherChecked = false;
                resources.setUnitSystem(currentUnitSystem);
                break;
            case 3:
                appOptions();
            default:
                weatherUnitsOptionsChoice();
        }

        appOptions();
    }

    //Application Option -> Change User ID/ Restore default User ID
    private void userIdOptions() throws IOException, InterruptedException {

        screenCleaner();
        logo.showLogo();

        System.out.println("USER ID OPTIONS:\n"
                + "1. Zmień ID\n"
                + "2. Przywróć domyślne ID\n"
                + "3. Wróć do opcji\n"
                + "\n");
        System.out.println("Wybierz odpowiednią opcję i naciśnij [enter]: ");

        userIdOptionsChoice();
    }

    private void userIdOptionsChoice() throws IOException, InterruptedException {
        int userIdOptionsChoice = 0;

        try {
            System.out.print("=> ");
            userIdOptionsChoice = new Scanner(System.in).nextInt();
            executeUserIdOptions(userIdOptionsChoice);
        } catch (InputMismatchException exc) {
            executeUserIdOptions(userIdOptionsChoice);
        }

    }

    private void executeUserIdOptions(int userIdChoice) throws IOException, InterruptedException {

        switch (userIdChoice) {
            case 1:
                setUserId();
                break;
            case 2:
                resetUserId();
                break;
            case 3:
                appOptions();
                break;
            default:
                userIdOptionsChoice();
                break;
        }

        appOptions();
    }

    private void setUserId() throws IOException, InterruptedException {
        String newUserId;
        String fileName = "resources\\userid.txt";

        System.out.println("UWAGA! Wpisanie błędnego id może spowodować nieoczekiwane błędy!\n"
                + "Sprawdź dwa razy zanim zatwierdzisz!"
                + "Jeżeli nie jesteś pewny swojego ID, naciśnij Q aby wrocić do opcji.");
        System.out.print("Wpisz swoje ID: ");

        newUserId = new Scanner(System.in).nextLine();

        if (newUserId.equalsIgnoreCase("q")) {
            appOptions();
        } else if (newUserId.length() == 32) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
                resources.setUserId(newUserId);
                bw.write(newUserId);
                bw.flush();
                bw.close();
            } catch (IOException exc) {
                System.out.println("Nie mogę ustawić twojego ID!");
            }
            appOptions();
        } else {
            System.out.println("Podałeś prawdopodobnie błędne ID, sprawdź czy zawiera 32 znaki i spróbuj ponownie!");
            pressEnterToContinue();
            appOptions();
        }
    }

    private void resetUserId() throws IOException {
        String defaultUserId;
        String fileReadName = "resources\\defaultid.txt";
        String fileWriteName = "resources\\userid.txt";

        try (BufferedReader cityReader = new BufferedReader(new FileReader(fileReadName))) {
            while ((defaultUserId = cityReader.readLine()) != null) {
                resources.setUserId(defaultUserId);
            }
        } catch (IOException exc) {
            System.out.println("Błąd wczytywania pliku defaultcity.txt");
        } finally {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileWriteName))) {
                String newUserId = resources.getUserId();
                bw.write(newUserId);
                bw.flush();
                bw.close();
            } catch (IOException exc) {
                System.out.println("Błąd przepisania domyślnego ID do twojego ID");

            }
        }

        System.out.println("Przywrócono domyślne ID");
    }

    //Main Menu -> Read Me
    private void applicationInfo() throws IOException, InterruptedException {

        screenCleaner();
        logo.showLogo();

        System.out.println("\tOPIS DZIAŁANIA PROGRAMU");
        System.out.println("\n -- Skąd program bierze dane?"
                + "\n Program pobiera informacje pogodowe z serwisu OpenWeatherMap."
                + "\n W związku z powyższym pobrane dane mogą nieznacznie różnić się od atualnych lub"
                + "\n mogą być niedostępne chwilowo niedostępne (pojawi się stosowny komunikat o braku połączenia)"
                + "\n\n -- Czy jest jakiś limit pobierania danych?"
                + "\n Niestety TAK. Globalny limit dla tej aplikacji wynosi 50 zapytań na godzinę."
                + "\n Wydaje się dużo, jednak każda zmiana domyślnego miasta, zmiana jednostek lub sprawdzanie"
                + "\n pogody w kolejnych miastach generuje kolejne zapytanie do serwisu. Nadmiera ilość zapyań"
                + "\n może doprowadzić do nieoczekiwanych błędów: nie pobierze danych z serwera, w związku z tym"
                + "\n zamknie aplikacje"
                + "\n Skoro program pobieda dane z zewnętrznej witryny, to jak podawać nazwę miasta?"
                + "\n Nazwę miasta podajemy bez jakichkolwiek znaków dialektycznych polskich czy innych."
                + "\n Dodatkowo podanie losowego ciągu znaków niekoniecznie wywoła błąd. System wyszukiwania"
                + "\n z serwisu pogodowego prawdopodobnie znajdzie miasto o zbliżonej nazwie i wyświetli wyniki."
                + "\n np. 'zxczxaee' rozpozna jako Tallinn w Estonii, a 'gadx&*%*aaksl' będzie Koukkuniemi w Finlandii"
                + "\n\n -- Co jest potrzebne do działania programu?"
                + "\n Program WYMAGA dostępu do Internetu. W przeciwnym wypadku wyłączy się automatycznie"
                + "\n aby nie ryzykować uszkodzenia lub niechcianego nadpisania danych w folderze resources"
                + "\n\n -- Uwagi:"
                + "\n Przy podaniu błednego ID użytkownika (podczas zmiany z domyślnego na własny) program,"
                + "\n aby uchronić aplikację przed crashem, zeruje wartość domyślnego miasta. W takim przypadku"
                + "\n należy ustawić domyślne miasto od nowa");

        pressEnterToContinue();
        showMainMenu();

    }

    //Show/Chceck Weather methods    
    private void showWeatherForDefaultCity(String thisCityName) throws IOException {
        if (defCityWeatherChecked == true) {
            getWeatherForCity();
        } else if (defCityWeatherChecked == false) {
            getResponseAsString(thisCityName);
            getWeatherForCity();
            defCityWeatherChecked = true;
        }
    }

    private void showWeatherForCity(String thisCityName) throws IOException {
        getResponseAsString(thisCityName);
        getWeatherForCity();
    }

    private void getWeatherForCity() throws IOException { //Do wyrzucenia do klasy Weather

        try {
            Weather weather = new Weather(responseString);

            System.out.print(weather.getCityNameFromAPI() + ", " + weather.getCountrySymbolFromAPI());
            System.out.println(" - " + weather.getWeathernfo() + "\t\t jednostki: " + resources.getUnitSysForUser());
            System.out.println("\n\tTemperatura: " + weather.getTemperature() + resources.getTemperatureUnit() +
                    "\t\t" + "Ciśnienie: " + weather.getPressure() + " hPa");
            System.out.println("\tTemp. min.: " + weather.getMinTemperature() + resources.getTemperatureUnit() +
                    "\t\t" + "Wilgotność: " + weather.getHumidity() + "%");
            System.out.println("\tTemp. max.: " + weather.getMaxTemperature() + resources.getTemperatureUnit() +
                    "\t\t" + "Siła wiatru: " + weather.getWind() + resources.getWindSpeedUnit());

        } catch (ArrayIndexOutOfBoundsException exc) {
            System.out.println("\nBrak miasta domyślnego, lub takie miasto nie istnieje");
            city.setDefaultCity("-");
        }
    }

    //Weather internet methods
    private String getResponseAsString(String thisCityName) {
        response = resources.getFRONT_OF_ADRESS() + thisCityName + resources.getUnitSysForUrl()
                + resources.getAPP_ID() + resources.getUserId() + resources.getLANG();

        try {
            responseString = Unirest.get(response).asString().getBody();

        } catch (UnirestException exc) {
            resoponseError(thisCityName);
        }
        return responseString;
    }

    private void resoponseError(String thisCityName) {
        if (internetConnectionErrorCounter > 0) {
            System.out.println("Błąd pobierania danych. Sprawdź połączenie internetowe " +
                    "lub serwis pogodowy jest niedostępny");
            System.out.println("Naciśnij [ENTER] aby ponowić próbę (pozosło prób "
                    + internetConnectionErrorCounter + ")");

            try {
                System.in.read();
            } catch (IOException exc) {
            }

            --internetConnectionErrorCounter;
            getResponseAsString(thisCityName);

        } else {
            System.out.println("BRAK WYMAGANEGO POŁĄCZENIA Z INTERNETEM "
                    + "LUB SERWIS POGODOWY JEST NIEDOSTĘPNY"
                    + "\nPROGRAM KOŃCZY DZIAŁANIE");
            System.exit(0);
        }
    }

    //Other Methods
    private void screenCleaner() throws IOException, InterruptedException {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (IOException | InterruptedException exc) {
            System.out.println("Nie można wyczyścić ekranu, przepraszamy!");
        }
    }

    private void pressEnterToContinue() {
        System.out.println("\n --- Naciśnij [ENTER] aby wrócić do menu głównego ---");

        try {
            System.in.read();
        } catch (IOException exc) {
        }

    }

}