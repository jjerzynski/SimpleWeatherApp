package weatherapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Jakub Jerzyński
 */
public class Logo {
    
    Logo(){
    }
    
    private void logo(){
        String logo;
        
        String fileName = "resources" + File.separator + "logo.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((logo = br.readLine()) != null) {
                System.out.println(logo);
            }
            
            br.close();
        } catch (IOException exc) {
            System.out.println("błąd odczytu pliku logo");
        }
    }
        
    public void showLogo(){
        
        System.out.println("\n-----------------------------------------------------------------------------\n");
                logo();
        System.out.println("\n-----------------------------------------------------------------------------\n");
    }
}
