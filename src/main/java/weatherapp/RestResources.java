package weatherapp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Jakub Jerzyński
 */

class RestResources {

        private final String FRONT_OF_ADRESS = "http://api.openweathermap.org/data/2.5/weather?q=";
        private final String APP_ID = "&appid=";
        private final String LANG = "&lang=pl";
        private String userId;
        private String unitSysForUrl;
        private String unitSysForUser;
        private String temperatureUnit;
        private String windSpeedUnit;
  
    protected RestResources() {
        
        this.unitSysForUrl = "&units=metric";
        this.unitSysForUser = "metryczne";
        this.temperatureUnit = " st.C";
        this.windSpeedUnit = " km/h";
        readUserIdFromFile();
    }
    

    protected RestResources(String unitSystem){
        readUserIdFromFile();
        setUnitSystem(unitSystem);
    }
    
        
    protected String getFRONT_OF_ADRESS() {
        return FRONT_OF_ADRESS;
    }

    protected String getAPP_ID() {
        return APP_ID;
    }

    public String getLANG() {
        return LANG;
    }
    
    protected String getUserId() {
        return userId;
    }

    protected void setUserId(String userId) {
        this.userId = userId;
    }

    protected String getTemperatureUnit() {
        return temperatureUnit;
    }

    protected String getWindSpeedUnit() {
        return windSpeedUnit;
    }

    protected final void setUnitSystem(String unitSystem){
        if(unitSystem.equalsIgnoreCase("metric")) {
            this.unitSysForUrl = "&units=metric";
            this.unitSysForUser = "metryczne";
            this.temperatureUnit = " st. C";
            this.windSpeedUnit = " km/h";
        }
        else if(unitSystem.equalsIgnoreCase("imperial")){
            this.unitSysForUrl = "&units=imperial";
            this.unitSysForUser = "imperialne";
            this.temperatureUnit = " st. F";
            this.windSpeedUnit = " mph";
        }
    }

    protected String getUnitSysForUser() {
        return unitSysForUser;
    }

    protected String getUnitSysForUrl() {
        return unitSysForUrl;
    } 
    
    private void readUserIdFromFile(){
        String readedId;
        
        try (BufferedReader cityReader = new BufferedReader(new FileReader("resources\\userid.txt"))) {
            while ((readedId = cityReader.readLine()) != null) {
                this.userId = readedId;
            }
        } catch (IOException exc) {
            System.out.println("Błąd wczytywania pliku userid.txt");
        }  
    }
}
