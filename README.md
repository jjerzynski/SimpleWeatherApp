## WeatherApp

WeatherApp jest prostą aplikacją napisaną zanim poznałem dokładniej RESTa, JSONa w tym bibliotekę Jackson, 
a także przed poznaniem testów jednostkowych, dodatkowo GITa dopiero poznawałem.

Jej zadaniem jest łączenie sie z API [openweathermap](http://openweathermap.org/), wyciągnięcie interesujących nas danych
i wyświetlenie ich w konsoli.

- Spis treści

1. O aplikacji
2. Co bym zmienił pisząc od nowa
3. Screeny z aplikacji
4. Linki do pozostałych projektów

#### O aplikacji:

Aplikacja posiada konsolowe menu, dzięki któremu możemy wybrać:
- domyślne miasto, sprawdzane przy starcie aplikacji
- system jednostek (metryczne lub imperialne)
- zmienić i ustawić klucz użytkownika na własny
- sprawdzać pogodę dla wybranego miasta

Dodatkowo:
- Aplikacja wymaga dostępu do internetu aby działała. W przeciwnym wypadku wyświetli
komunikat o braku połączenia i po trzech nieudanych próbach aplikacja zakończy działanie.
- Aplkacja wymaga* posiadania własnego ID ze strony openweathermap.com, szczegóły
pod tym linkiem: http://openweathermap.org/appid
- W przypadku wystąpienia "błędu krytycznego", aplikacja "resetuje" swoje ustawienia. Jeżeli nie nastąpi reset, można go wywołać ręcznie
w opcjach.


#### Co bym zmienił pisząc to od nowa
Z pewnością wiele można tej appce zarzucić. Błędy projektowe, błędy w pisaniu kodu, brak testów, brak przechwytywania błędów. 
Dlatego po skończeniu innego projektu (równiez dostępnego na moim repozytorium GITa o nazwie Simple RPG game) napiszę nową, być może 
okienkową aplikacje pogodową.

Już teraz wiem, że z pewnością użyłbym mi. następujących bibliotek:
- Uniresta - do łączenia się z API
- Jacksona - dla lepszego i łatwiejszego odczytywania danych, a także do tworzenia ze zwracanego JSONa obiektów pogodowych
- JUnita - dla testów

i Mavena oczywiście, by to wszystko powiązać ze sobą.


#### Kilka screenow z appki:

- Menu główne

[![Menu główne](https://i.imgur.com/nFjhsLJ.png)](https://i.imgur.com/nFjhsLJ.png)

- Opcje

[![Opcje](https://i.imgur.com/l1fmAL7.png)](https://i.imgur.com/l1fmAL7.png)

- Sprawdzenie pogody dla innego miasta niż domyślne

[![Pogoda](https://i.imgur.com/UR6QyQb.png)](https://i.imgur.com/UR6QyQb.png)

#### Moje opublikowane projekty
1. [Simple RPG Game](https://gitlab.com/jjerzynski/SimpleRpgGame) - (w produkcji) Okienkowa gra RPG stylizowana na produkcje z lat '80-'90, mój największy projekt;
2. [Weather App](https://gitlab.com/jjerzynski/SimpleWeatherApp) - (projekt zakończony) Konsolowa aplikacja pogodowa oparna na REST;
3. [Weather App 2](https://gitlab.com/jjerzynski/RESTWeatherApp) - (w produkcji) Ulepszona aplikacja pogodowa w oparciu o REST i JSON oraz Swinga;
4. [Mastermind](https://gitlab.com/jjerzynski/MasterMindConsole) - (projekt zakończony) Gra logiczna z lat '80;
5. [TicTacToe](https://gitlab.com/jjerzynski/TicTacToeGame) - (projekt zakończony) Kóło i krzyżyk - projekt na zaliczenie zajęc w Craftin' Code, mój pierwszy projekt;
